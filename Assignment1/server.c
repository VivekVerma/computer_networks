/****************** SERVER CODE ****************/
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>

void die(char *errorMessage) // To print error messages
{
  perror(errorMessage);
  exit(1);
}

void SendWelcomeMessage(FILE* in, char* buffer){ // To send the initial welcome message to client
	strcpy(buffer,"0022Welcome to my World!\n");
	fwrite(buffer,1,4096,in);
	fflush(in);
}

void SendEmptyPacket(FILE* in, FILE*fp, char* buffer){
	sprintf(buffer,"0000");
	fwrite(buffer,1,4096,in);
	fflush(in);  
	fclose(fp);
}

void SendFileData(FILE* in, FILE* fp, char* buffer){ // To send the file contents in chunk to client
	int bytesRead;
	while((bytesRead=fread(buffer+4,1, 4092, fp))>0){
		char store=buffer[4];
		sprintf(buffer,"%04d",bytesRead);
		buffer[4]=store;
		fwrite(buffer,1,4096,in);
		fflush(in);  
		printf("%d Bytes Sent!\n",bytesRead);
		bzero(buffer,4096);
	}
	SendEmptyPacket(in, fp, buffer);
}

int CheckIfFileExists(FILE* in, FILE* fp, char* buffer){ // Checks if the requested file exists
	if (fp==NULL){
		strcpy(buffer,"X022File does not exist!\n");
		fwrite(buffer,1,4096,in);
		fflush(in);
		printf("File does not exist!\n");
		return 1;
	}
	return 0;
}

void AskForFileName(FILE* in, char* buffer){ // Prompt for the filename
	strcpy(buffer,"0018Enter File name: ");
	fwrite(buffer,1,4096,in);
	fflush(in);
}

int main(int argc, char *argv[]){
	if (argc!=2) die("Port number?\n");
	
	struct sockaddr_in serverAddress;
	struct sockaddr_storage serverStorage;
	socklen_t addressSize;
	int mainSocket, clientSocket, flag;
	char buffer[4096];
	FILE *fp;

	// Create a socket
	mainSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (mainSocket<0) die("socket() failed");

	// Initializing the parameters
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(atoi(argv[1]));
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	memset(serverAddress.sin_zero, '\0', sizeof serverAddress.sin_zero);  

	// Binding to the socket
	if(bind(mainSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress))) die("bind() failed");

	while(1){
		// Listening for clients with upto 5 connections in queue
		if(listen(mainSocket,5)==0)
			printf("Listening\n");
		else
			printf("Error\n");

		// Accepting the connection from client
		addressSize = sizeof serverStorage;
		clientSocket = accept(mainSocket, (struct sockaddr *) &serverStorage, &addressSize);
		
		printf("Connection established\n");
		FILE* in = fdopen(clientSocket, "r+");

		SendWelcomeMessage(in, buffer);

		flag=0;

		while(1){
			if (feof(in)) break;
			AskForFileName(in, buffer);
			while (fread(buffer,1,4096,in)<=0){
				if (feof(in)){ // Check if client exited
				flag=1;
				break;
			}
		}
		if (flag) break;  // Check if client exited

		printf("Request for %s\n",buffer);
		
		fp = fopen(buffer, "rb");
		if (CheckIfFileExists(in, fp, buffer)) continue;
		
		SendFileData(in, fp, buffer);
		}
		
		fclose(in);
	}

	return 0;
}
