/****************** CLIENT CODE ****************/

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>

void die(char *errorMessage) // To print error messages
{
  perror(errorMessage);
  exit(1);
}

int GetSizeToRead(char* buffer){ // Get the size to read in each packet using the packet header
	char subbuffer[5];
	memcpy( subbuffer, buffer, 4 );
	subbuffer[4] = '\0';
	return atoi(subbuffer);
}

int CheckForError(char* buffer){ // Check for any error messages in the packet
	if (buffer[0]=='X'){
		buffer[0]='0';
		return 1;
	}
	return 0;
}

void GetFileData(FILE* out, char* buffer){ // Get file data from server
	int count=0, error=0, length;
	while(1){
		fread(buffer, 1, 4096, out);

		if (CheckForError(buffer)) error=1;

		length = GetSizeToRead(buffer);
		if (length==0) return;

		if (count==0 && error!=1) printf("\nData received:\n");

		count++;
		fwrite(buffer+4,1,length,stdout);
		fflush(stdout);

		if (error==1) return;
	}
}

void GetResponse(FILE* out, char* buffer){ // Get a single response from server
	fread(buffer, 1, 4096, out);
	buffer[4+GetSizeToRead(buffer)]='\0';
	printf("\n%s",buffer+4);
}

void SendMessage(FILE* out, char* buffer){ // Send a message to the server
	scanf("%s",buffer);
	fwrite(buffer, 1, 4096, out);
	fflush(out);
}

int main(int argc, char *argv[]){
	if (argc!=3) die("Server IP address and Port number?\n");
	
	struct sockaddr_in serverAddress;
	socklen_t addressSize;
	int clientSocket, length, error=0, count=0;
	char buffer[4096];

	// Create a socket
	clientSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (clientSocket<0) die("socket() failed");

	// Initializing the parameters
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(atoi(argv[2]));
	serverAddress.sin_addr.s_addr = inet_addr(argv[1]);
	memset(serverAddress.sin_zero, '\0', sizeof serverAddress.sin_zero);  
	
	printf("Waiting for server!\n\n");
	
	// Connecting to the server
	addressSize = sizeof serverAddress;
	if(connect(clientSocket, (struct sockaddr *) &serverAddress, addressSize)) die("connect() failed");

	FILE* out = fdopen(clientSocket, "r+");

	GetResponse(out, buffer);

	while(1){
		if (feof(out)){ // Checking if server exited
			printf("Connection closed\n");
			break;
		}
		GetResponse(out, buffer);
		SendMessage(out, buffer);
		GetFileData(out, buffer);
	}

	fclose(out);
	return 0;
}
