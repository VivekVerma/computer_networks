/****************** STCP CODE ****************/

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <arpa/inet.h>
#include "mysock.h"
#include "stcp_api.h"
#include "transport.h"
#include <time.h>
#include <sys/time.h>

#define MAX_WAIT_TIME 10
#define MAX_WINDOW_SIZE 3072

enum {
    CSTATE_CLOSED,
    CSTATE_ESTABLISHED,
    CSTATE_FIN_WAIT1,
    CSTATE_FIN_WAIT2,
    CSTATE_CLOSE_WAIT,
    CSTATE_TIME_WAIT,
    CSTATE_LAST_ACK,
    CSTATE_CLOSING
};

typedef struct{
  STCPHeader header;
  char payload[STCP_MSS];
} STCPPacket;

/* this structure is global to a mysocket descriptor */
typedef struct
{
    bool_t done;    /* TRUE once connection is closed */

    int connection_state;   /* state of the connection (established, etc.) */
    tcp_seq initial_sequence_num;

    tcp_seq next_expected; // The sequence number I am expecting to be received

    tcp_seq next_send; // The sequence number I am going to send next

    tcp_seq peer_seq; // The sequence number of the latest packet from peer

    uint16_t my_window;

    uint16_t peer_window;

    int wait_for;

} context_t;

static void generate_initial_seq_num(context_t *ctx);
static void control_loop(mysocket_t sd, context_t *ctx);
void our_dprintf(const char *format, ...);

/* initialise the transport layer, and start the main loop, handling
 * any data from the peer or the application.  this function should not
 * return until the connection is closed.
 */
void transport_init(mysocket_t sd, bool_t is_active)
{
    context_t *ctx;

    ctx = (context_t *) calloc(1, sizeof(context_t));
    assert(ctx);

    generate_initial_seq_num(ctx);
    ctx->connection_state = CSTATE_CLOSED;
    ctx->my_window = MAX_WINDOW_SIZE;

    if (is_active){ // Client

        // Send SYN

        STCPHeader* syn_packet = (STCPHeader*)malloc(sizeof(STCPHeader));
      	syn_packet->th_seq = htonl(ctx->initial_sequence_num);
        syn_packet->th_flags = TH_SYN;
        syn_packet->th_off = 5;
      	stcp_network_send(sd, syn_packet, sizeof(STCPHeader), NULL);

        // Wait for SYN-ACK
        STCPHeader* syn_ack_packet = (STCPHeader*)malloc(sizeof(STCPHeader));
        stcp_wait_for_event(sd, NETWORK_DATA, NULL);
    		stcp_network_recv(sd, syn_ack_packet, sizeof(STCPHeader));
        if (!((syn_ack_packet->th_flags & TH_SYN) && (syn_ack_packet->th_flags & TH_ACK))) errno = ECONNREFUSED; // If wrong flag packet
        if (ntohl(syn_ack_packet->th_ack) != ntohl(syn_packet->th_seq) + 1) errno = ECONNREFUSED; // Checking if the numbers are in order

        if (errno != ECONNREFUSED){
            // Send ACK for SYN-ACK

            STCPHeader* ack_syn_ack_packet = (STCPHeader*)malloc(sizeof(STCPHeader));
            ack_syn_ack_packet->th_seq = htonl(ntohl(syn_ack_packet->th_ack));
          	ack_syn_ack_packet->th_ack = htonl(ntohl(syn_ack_packet->th_seq) + 1);
          	ack_syn_ack_packet->th_flags = TH_ACK;
            ack_syn_ack_packet->th_win = htons(ctx->my_window);
            ack_syn_ack_packet->th_off = 5;
            stcp_network_send(sd, ack_syn_ack_packet, sizeof(STCPHeader), NULL);
            ctx->next_send = ntohl(syn_ack_packet->th_ack);
            ctx->next_expected = ctx->next_send;
            ctx->peer_window = MIN(ntohl(syn_ack_packet->th_win), MAX_WINDOW_SIZE);
            ctx->peer_seq = ntohl(syn_ack_packet->th_seq);

            free(ack_syn_ack_packet);
        }
        free(syn_ack_packet);
        free(syn_packet);

    }

    else { // Server

        // Waiting for SYN

        STCPHeader* syn_packet = (STCPHeader*)malloc(sizeof(STCPHeader));
        stcp_wait_for_event(sd, NETWORK_DATA, NULL);
    		stcp_network_recv(sd, syn_packet, sizeof(STCPHeader));
        if (!(syn_packet->th_flags & TH_SYN)) errno = ECONNREFUSED; // If wrong flag packet

        if (errno != ECONNREFUSED){
            // Sending  SYN-ACK

          	STCPHeader* syn_ack_packet = (STCPHeader*)malloc(sizeof(STCPHeader));
          	syn_ack_packet->th_seq = htonl(ctx->initial_sequence_num);
          	syn_ack_packet->th_ack = htonl(ntohl(syn_packet->th_seq) + 1);
          	syn_ack_packet->th_flags = TH_SYN | TH_ACK;
            syn_ack_packet->th_win = htons(ctx->my_window);
            syn_ack_packet->th_off = 5;
            stcp_network_send(sd, syn_ack_packet, sizeof(STCPHeader), NULL);

        		// Waiting for ACK for SYN-ACK

            STCPHeader* ack_syn_ack_packet = (STCPHeader*)malloc(sizeof(STCPHeader));
            stcp_wait_for_event(sd, NETWORK_DATA, NULL);
    		    stcp_network_recv(sd, ack_syn_ack_packet, sizeof(STCPHeader));
            if (!(ack_syn_ack_packet->th_flags & TH_ACK)) errno = ECONNREFUSED; // If wrong flag packet
            if (ntohl(ack_syn_ack_packet->th_ack) != ntohl(syn_ack_packet->th_seq) + 1) errno = ECONNREFUSED; // Checking if the numbers are in order

        		ctx->next_send = ntohl(ack_syn_ack_packet->th_ack);
            ctx->next_expected = ctx->next_send;
            ctx->peer_window = MIN(ntohs(ack_syn_ack_packet->th_win), MAX_WINDOW_SIZE);
            ctx->peer_seq = ntohl(ack_syn_ack_packet->th_seq);

            free(ack_syn_ack_packet);
            free(syn_ack_packet);
            // 3 way handshake done!
        }
        free(syn_packet);
    }
    if (errno != ECONNREFUSED){
        ctx->connection_state = CSTATE_ESTABLISHED;
        ctx->done = 0; // Connection opened
        stcp_unblock_application(sd);
        control_loop(sd, ctx);
    }

    free(ctx);
}


/* generate random initial sequence number for an STCP connection */
static void generate_initial_seq_num(context_t *ctx)
{
    assert(ctx);

#ifdef FIXED_INITNUM
    /* please don't change this! */
    ctx->initial_sequence_num = 1;
#else
    struct timeval time;
    gettimeofday(&time,NULL);
    srand((time.tv_sec * 1000) + (time.tv_usec / 1000)); // Random number generator seeding
    ctx->initial_sequence_num = rand() % 256;
#endif
}


/* control_loop() is the main STCP loop; it repeatedly waits for one of the
 * following to happen:
 *   - incoming data from the peer
 *   - new data from the application (via mywrite())
 *   - the socket to be closed (via myclose())
 *   - a timeout
 */
static void control_loop(mysocket_t sd, context_t *ctx)
{
    assert(ctx);
    assert(!ctx->done);

    unsigned int event;
    size_t size=0, size_to_send=0, send_done=0;
    char data[MAX_WINDOW_SIZE];
    STCPPacket data_packet;

    ctx->wait_for = ANY_EVENT;
    while (!ctx->done)
    {
        ctx->peer_window = MIN(MAX_WINDOW_SIZE, ctx->peer_window);
        assert(ctx->next_send >= ctx->next_expected); // Checking if the expected ack number is less than or equal to the last sequence number
        event = stcp_wait_for_event(sd, ctx->wait_for, NULL);

        if (event & NETWORK_DATA){ // Network Event
            size = stcp_network_recv(sd, &data_packet, sizeof(STCPPacket)) - sizeof(STCPHeader);
            assert(ctx->peer_seq <= ntohl(data_packet.header.th_seq) + size); // Checking if peer is sending the data in increasing order of sequence number
            ctx->peer_seq = MAX(ntohl(data_packet.header.th_seq),ctx->peer_seq); // Updating the seq number of peer in our context
            ctx->peer_window = ntohs(data_packet.header.th_win); // Updating the peer window size`

          if (data_packet.header.th_flags & TH_FIN){ // Connection Termination
              if (data_packet.header.th_flags & TH_ACK) ctx->next_expected = ntohl(data_packet.header.th_ack);
              stcp_fin_received(sd); // Sending signal to app telling that a FIN packet arrived
              if(ctx->connection_state == CSTATE_ESTABLISHED) ctx->connection_state = CSTATE_CLOSE_WAIT;
              else if(ctx->connection_state == CSTATE_FIN_WAIT2) ctx->connection_state = CSTATE_TIME_WAIT;
              else if(ctx->connection_state == CSTATE_FIN_WAIT1) ctx->connection_state = CSTATE_CLOSING; // Simultaneous FIN Packets
              data_packet.header.th_flags = TH_ACK; // Acknowledging the FIN Packet arrived
              data_packet.header.th_ack = htonl(ctx->peer_seq);
              data_packet.header.th_off = 5;
              if (ctx->connection_state == CSTATE_CLOSE_WAIT || ctx->connection_state == CSTATE_TIME_WAIT) data_packet.header.th_seq = htonl(ctx->next_send);
              else if (ctx->connection_state == CSTATE_CLOSING) data_packet.header.th_seq = htonl(ctx->next_send+1);
              stcp_network_send(sd, &data_packet, sizeof(STCPHeader), NULL); // Sending the ACK for FIN Packet
          }
          else if (data_packet.header.th_flags & TH_ACK){ // Acknowledgement
              if(ctx->connection_state == CSTATE_FIN_WAIT1) ctx->connection_state = CSTATE_FIN_WAIT2;
              else if(ctx->connection_state == CSTATE_LAST_ACK) ctx->connection_state = CSTATE_CLOSED; // Last ACK for the FIN Packet
              else if(ctx->connection_state == CSTATE_CLOSING) ctx->connection_state = CSTATE_TIME_WAIT;
              ctx->next_expected = ntohl(data_packet.header.th_ack);
          }
          if (size > 0){ // Sending ACK for received data and forwarding data to app
              memcpy(data, data_packet.payload, size);
              stcp_app_send(sd, data, size);
              ctx->peer_seq = ntohl(data_packet.header.th_seq) + size;
              data_packet.header.th_flags = TH_ACK;
              data_packet.header.th_ack = htonl(ctx->peer_seq);
              data_packet.header.th_seq = htonl(ctx->next_send);
              data_packet.header.th_win = htons(ctx->my_window);
              data_packet.header.th_off = 5;
              stcp_network_send(sd, &data_packet, sizeof(STCPHeader), NULL);
          }
        }
        else if (event & APP_DATA){
            size = stcp_app_recv(sd, data, ctx->peer_window);
            send_done = 0;
            while(size>0){ // Sending data in chunks from app to network
                size_to_send = MIN(size,STCP_MSS); // So that we handle the last chunk
                data_packet.header.th_seq = htonl(ctx->next_send);
                data_packet.header.th_ack = htonl(ctx->peer_seq);
                data_packet.header.th_win = htons(ctx->my_window);
                data_packet.header.th_flags = TH_ACK;
                data_packet.header.th_off = 5;
                memcpy(data_packet.payload, data + send_done, size_to_send);
                stcp_network_send(sd, &data_packet, sizeof(STCPHeader) + size_to_send, NULL);
                ctx->next_send += size_to_send;
                send_done += size_to_send;
                size -= size_to_send;
            }
        }
        else if (event & APP_CLOSE_REQUESTED){ // Sending the FIN Packet to peer
            if (ctx->connection_state == CSTATE_ESTABLISHED) ctx->connection_state = CSTATE_FIN_WAIT1;
            if (ctx->connection_state == CSTATE_CLOSE_WAIT) ctx->connection_state = CSTATE_LAST_ACK;
            data_packet.header.th_flags = TH_FIN | TH_ACK;
            data_packet.header.th_seq = htonl(ctx->next_send);
            data_packet.header.th_ack = htonl(ctx->peer_seq);
            data_packet.header.th_win = htons(ctx->my_window);
            data_packet.header.th_off = 5;
            stcp_network_send(sd, &data_packet, sizeof(STCPHeader), NULL);
            ctx->wait_for = (NETWORK_DATA | APP_CLOSE_REQUESTED | TIMEOUT); //  No more App data
            ctx->next_send++;
        }

        if (ctx->connection_state == CSTATE_TIME_WAIT){ // Could have used sleep here but ignoring it as network layer is reliable
            ctx->connection_state = CSTATE_CLOSED;
        }
        if (ctx->connection_state == CSTATE_CLOSED) ctx->done=1; // We are done here
    }
}

/**********************************************************************/
/* our_dprintf
 *
 * Send a formatted message to stdout.
 *
 * format               A printf-style format string.
 *
 * This function is equivalent to a printf, but may be
 * changed to log errors to a file if desired.
 *
 * Calls to this function are generated by the dprintf amd
 * dperror macros in transport.h
 */
void our_dprintf(const char *format,...)
{
    va_list argptr;
    char buffer[1024];

    assert(format);
    va_start(argptr, format);
    vsnprintf(buffer, sizeof(buffer), format, argptr);
    va_end(argptr);
    fputs(buffer, stdout);
    fflush(stdout);
}
