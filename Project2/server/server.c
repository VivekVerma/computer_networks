/****************** SERVER CODE ****************/
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "proxy_parse.h"
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>

#define BUFFER_SIZE 4096

extern int errno;

char *base_directory="";
char serverName[32];

void die(char *errorMessage){ // To print error messages
  perror(errorMessage);
  exit(1);
}

int is_directory(const char *path){ // Check if a directory
    struct stat pathStat;
    if(lstat(path, &pathStat)!=0) return 1;
    return S_ISDIR(pathStat.st_mode);
}

char* GetKeyValue(struct ParsedRequest* http_request, char* requestedKey){ // Get Key Value pair for the request headers
	for(int i=0; i<http_request->headerslen; i++){
		if (!strcmp(http_request->headers[i].key,requestedKey)){
			return http_request->headers[i].value;
		}
	}
	return NULL;
}

const char *get_filename_ext(const char *filename) { // get extenstion of filename
    char *ext;

    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "text/html";
    ext = dot + 1;

	if (!strcmp("html",ext) )return "text/html";
	else if (!strcmp("htm",ext)) return "text/html";
	else if (!strcmp("txt",ext)) return "text/plain";
	else if (!strcmp("jpeg",ext)) return "image/jpeg";
	else if (!strcmp("jpg",ext)) return "image/jpeg";
	else if (!strcmp("gif",ext)) return "image/gif";
	else if (!strcmp("pdf",ext)) return "application/pdf";
	else return "application/octet-stream";
}

const char *get_error_reason(int errorCode) { //Mapping b/t error Code and error String
	if (errorCode==200) return "OK";
	else if (errorCode==400) return "Bad Request";
	else if (errorCode==404) return "Not Found";
	else if (errorCode==500) return "Internal Server Error";
	else if (errorCode==501) return "Not Implemented";
}

void ErrorResponse(FILE* in, char* buffer, int errorCode, char *message){ // Used to respond with error codes
	char timebuf[64];
	time_t now;
	struct tm tm;
	int size=0;

	now = time(0);
	tm = *gmtime(&now);
	strftime(timebuf, sizeof timebuf, "%a, %d %b %Y %H:%M:%S %Z", &tm);
	bzero(buffer,BUFFER_SIZE);

	size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"HTTP/1.1 %d %s\r\n",errorCode,get_error_reason(errorCode));
	size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"Date: %s\r\n",timebuf);
	size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"Connection: close\r\n");
	size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"Content-Length: %d\r\n",strlen(message));
	size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"Content-Type: text/html\r\n");
	size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"Server: %s\r\n",serverName);
	size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"\r\n");

	memcpy(buffer+size,message,strlen(message));

	fwrite(buffer,1,BUFFER_SIZE,in);
	fflush(in);

	bzero(buffer,BUFFER_SIZE);
}

int GetDirectoryListing(char *buffer, char *directory, char *path, FILE *in){ // TO get Directory listing with hyperlinks
	int size=0;
	DIR *dp;
	struct dirent *ep;
	dp = opendir (directory);

	char *dirr = strstr(path,"/");
	if(dirr[strlen(dirr)-1]=='/') dirr[strlen(dirr)-1]='\0';

	if (dp != NULL){

		size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"<HTML><HEAD><TITLE>%s</TITLE></HEAD><BODY><H2>Contents of %s:</H2>",path,path);

		while (ep = readdir (dp)){
			size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"<A HREF=\"%s/%s\">%s</A><BR>",dirr,ep->d_name,ep->d_name);
		}

		size = size + snprintf(buffer+size,BUFFER_SIZE-strlen(buffer),"</BODY></HTML>");
		(void) closedir (dp);
	}

	else {
		return -1;
	}

	return size;
}

int ProcessGETRequest(struct ParsedRequest* http_request, char* buffer, FILE* in){ // THis process the GET request only

	int send, size=0, filesize=0, flag=0, errorCode=200;
	char string[BUFFER_SIZE], timebuf[128], *saveptr, requestedResource[1024], *fileToGet, contentType[16]="text/html", errorReason[16], errorMessage[256];
	time_t now;
	struct tm tm;
	FILE* fp;

	bzero(buffer,BUFFER_SIZE);

	strcpy(requestedResource,base_directory);
	strcat(requestedResource,http_request->path);
	if (strstr(http_request->path, "?")!=NULL){
		ErrorResponse(in, buffer, 501, "<HTML><HEAD><TITLE>501 Not Implemented</TITLE></HEAD><BODY><H1>501 Not Implemented</H1>The feature is not implemented.</BODY><HTML>");
		return 0;
	}

	fileToGet = strtok_r(requestedResource,"?",&saveptr);
	if (fileToGet==NULL) fileToGet = requestedResource;
	if (fileToGet[strlen(fileToGet)-1]=='/') fileToGet[strlen(fileToGet)-1]='\0';
	printf("Request: %s\n",http_request->path);

	// Checking if requested url is file, diretory with index.html, directory withour index.html or noting
	if (!(is_directory(fileToGet))){
		fp = fopen(fileToGet,"r");
		if (fp==NULL){
			filesize=0;
			errorCode=404;
		}
		else{
			fseek(fp, 0L, SEEK_END);
			filesize = ftell(fp);
			rewind(fp);
			strcpy(contentType, get_filename_ext(fileToGet));
		}
	}
	else{
		strcat(fileToGet,"/index.html");
		fp = fopen(fileToGet,"r");
		if (fp==NULL){
			fileToGet[strlen(fileToGet)-11]='\0';
			send = GetDirectoryListing(buffer,fileToGet,http_request->path,in);
			filesize = send;
			if (send<0){
				errorCode=404;
			}
		}
		else{
			fseek(fp, 0L, SEEK_END);
			filesize = ftell(fp);
			rewind(fp);
		}
	}

	now = time(0);
	tm = *gmtime(&now);
	strftime(timebuf, sizeof timebuf, "%a, %d %b %Y %H:%M:%S %Z", &tm);

	strcpy(errorReason, get_error_reason(errorCode));

	if (errorCode!=200){
		snprintf(errorMessage,256,"<HTML><HEAD><TITLE>%d %s</TITLE></HEAD><BODY><H1>%d %s</H1>The requested URL %s was not found on this server.</BODY><HTML>",errorCode,errorReason,errorCode,errorReason,fileToGet);
		ErrorResponse(in, buffer, errorCode, errorMessage);
		return 0;
	}

	while(filesize>0 || flag==0){

		size=0;
		bzero(string,BUFFER_SIZE);

		if (flag==0){ // Response Headers
			size = size + snprintf(string+size,BUFFER_SIZE-strlen(string),"HTTP/1.1 %d %s\r\n", errorCode, errorReason);
			size = size + snprintf(string+size,BUFFER_SIZE-strlen(string),"Date: %s\r\n",timebuf);
			size = size + snprintf(string+size,BUFFER_SIZE-strlen(string),"Connection: %s\r\n",GetKeyValue(http_request,"Connection"));
			size = size + snprintf(string+size,BUFFER_SIZE-strlen(string),"Content-Length: %d\r\n",filesize);
			size = size + snprintf(string+size,BUFFER_SIZE-strlen(string),"Content-Type: %s\r\n",contentType);
			size = size + snprintf(string+size,BUFFER_SIZE-strlen(string),"Server: %s\r\n",serverName);
			size = size + snprintf(string+size,BUFFER_SIZE-strlen(string),"\r\n");
			flag=1;
		}

		if (fp!=NULL) send = fread(buffer,1,BUFFER_SIZE-size,fp);
		memcpy(string+size,buffer,send); // Response Body

		filesize = filesize - send;

		fwrite(string,1,BUFFER_SIZE,in);
		fflush(in);
		bzero(buffer,BUFFER_SIZE);
	}

	if (fp!=NULL) fclose(fp);

	return 0;
}

int ProcessRequest(struct ParsedRequest* http_request, char* buffer, FILE* in){
	if (!strcmp(http_request->method,"GET")){
		if(ProcessGETRequest(http_request,buffer,in)!=0) die("GET Request Error\n");
	}
	else{
		ErrorResponse(in, buffer, 501, "<HTML><HEAD><TITLE>501 Not Implemented</TITLE></HEAD><BODY><H1>501 Not Implemented</H1>The feature is not implemented.</BODY><HTML>");
	}

	return 0;
}

void ConnectionHandler(void *clientSocketVoid){
	int bytesRead, flag=0, clientSocket = *(int *)clientSocketVoid;
	char buffer[BUFFER_SIZE];

	FILE* in = fdopen(clientSocket, "rb+");

	while(bytesRead = recv(clientSocket, buffer, BUFFER_SIZE, 0) > 0){
		struct ParsedRequest* http_request = ParsedRequest_create();
		if (ParsedRequest_parse(http_request, buffer, BUFFER_SIZE)<0){ // Error Response on Malformed Request
			ErrorResponse(in, buffer, 400, "<HTML><HEAD><TITLE>400 Malformed Request</TITLE></HEAD><BODY><H1>400 Malformed Request</H1>The http request is malformed.</BODY><HTML>");
			flag = 1;
		}

		else {
			if (ProcessRequest(http_request, buffer, in)!=0) flag=1;
			if (!strcmp(GetKeyValue(http_request,"Connection"),"close")) flag=1; // Close on Connection field value
		}
		ParsedRequest_destroy(http_request);
		if(flag){
			bytesRead=0;
			break;
		}
	}

	// Close on resetting or aborting connection by client
	if(bytesRead == 0)
    {
        printf("Connection Closed %d\n", clientSocket);
        fflush(stdout);
    }

    else if(bytesRead == -1 && (errno==ECONNRESET || errno==ECONNABORTED)){
		printf("Connection Closed %d\n", clientSocket);
        fflush(stdout);
    }

    else{
		ErrorResponse(in, buffer, 500, "<HTML><HEAD><TITLE>500 Internal Server Error</TITLE></HEAD><BODY><H1>500 Internal Server Error</H1>An error occured in the server.</BODY><HTML>");
    }

	free(clientSocketVoid);
	fclose(in);
}

int main(int argc, char *argv[]){
	if (argc!=3) die("Port number, Base Directory?\n");
	base_directory = argv[2];
	if (!is_directory(base_directory)) die("Enter Valid Base Directory");
	gethostname(serverName,sizeof(serverName));
	struct sockaddr_in serverAddress;
	struct sockaddr_storage serverStorage;
	socklen_t addressSize;
	int mainSocket, clientSocket, flag, *newSocket;
	char buffer[BUFFER_SIZE];
	FILE *fp;

	// Create a socket
	mainSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (mainSocket<0) die("socket() failed");

  if (setsockopt(mainSocket, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0) die("setsockopt(SO_REUSEADDR) failed");

	// Initializing the parameters
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(atoi(argv[1]));
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	memset(serverAddress.sin_zero, '\0', sizeof serverAddress.sin_zero);

	// Binding to the socket
	if(bind(mainSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress))) die("bind() failed");

	// Listening for clients with upto 5 connections in queue
	if(listen(mainSocket,5)==0)
		printf("Listening\n");
	else
		printf("Error\n");

	// Accepting the connection from client
	addressSize = sizeof serverStorage;
	while(clientSocket = accept(mainSocket, (struct sockaddr *) &serverStorage, &addressSize)){

		pthread_t clientThread;
		newSocket = malloc(sizeof(int*));
		*newSocket = clientSocket;

		// New thread for new connection
		if (pthread_create(&clientThread, NULL, ConnectionHandler, (void*)newSocket) < 0) die("pthread_create() failed");

		printf("Connection established %d\n", clientSocket);
		if (pthread_detach(clientThread)!=0) die("pthread_detach() error");

	}

	return 0;
}
