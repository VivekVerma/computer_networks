/****************** PROXY CODE ****************/
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "proxy_parse.h"
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <netdb.h>

#define BUFFER_SIZE 4096
#define MAX_PARALLEL_CONNECTION 20

extern int errno;

char serverName[32];
volatile int threadCount=0;

struct binary_semaphore {
    pthread_mutex_t mutex;
    pthread_cond_t cvar;
    int v;
};

struct binary_semaphore sem;

void mysem_post(struct binary_semaphore *p)
{
    pthread_mutex_lock(&p->mutex);
    if (p->v == 1)
        /* error */
    p->v += 1;
    pthread_cond_signal(&p->cvar);
    pthread_mutex_unlock(&p->mutex);
}

void mysem_wait(struct binary_semaphore *p)
{
    pthread_mutex_lock(&p->mutex);
    while (!p->v)
        pthread_cond_wait(&p->cvar, &p->mutex);
    p->v -= 1;
    pthread_mutex_unlock(&p->mutex);
}

void die(char *errorMessage){ // To print error messages
  perror(errorMessage);
  exit(1);
}

const char *get_error_reason(int errorCode) { //Mapping b/t error Code and error String
	if (errorCode==200) return "OK";
	else if (errorCode==400) return "Bad Request";
	else if (errorCode==404) return "Not Found";
	else if (errorCode==500) return "Internal Server Error";
	else if (errorCode==501) return "Not Implemented";
  else return "Unknown";
}

void ErrorResponse(int socket, char* buffer, int errorCode, char *message){ // Used to respond with error codes
	char timebuf[64];
	time_t now;
	struct tm tm;
	int size=0;

	now = time(0);
	tm = *gmtime(&now);
	strftime(timebuf, sizeof timebuf, "%a, %d %b %Y %H:%M:%S %Z", &tm);
	// bzero(buffer,BUFFER_SIZE);

	size = size + snprintf(buffer+size,BUFFER_SIZE-size,"HTTP/1.1 %d %s\r\n",errorCode,get_error_reason(errorCode));
	size = size + snprintf(buffer+size,BUFFER_SIZE-size,"Date: %s\r\n",timebuf);
	size = size + snprintf(buffer+size,BUFFER_SIZE-size,"Connection: close\r\n");
	size = size + snprintf(buffer+size,BUFFER_SIZE-size,"Content-Length: %d\r\n",strlen(message));
	size = size + snprintf(buffer+size,BUFFER_SIZE-size,"Content-Type: text/html\r\n");
	size = size + snprintf(buffer+size,BUFFER_SIZE-size,"Server: %s\r\n",serverName);
	size = size + snprintf(buffer+size,BUFFER_SIZE-size,"\r\n");

	memcpy(buffer+size,message,strlen(message));

	send(socket,buffer,size+strlen(message),0);
}

int hostname_to_ip(char * hostname , char* ip, int clientSocket, char* buffer){
  struct hostent *he;
  struct in_addr **addr_list;
  int i;

  if ((he = gethostbyname(hostname)) == NULL){
    // get the host info
    // die(hostname);
    return 1;
  }

  addr_list = (struct in_addr **) he->h_addr_list;

  for(i = 0; addr_list[i] != NULL; i++){
    //Return the first one;
    strcpy(ip , inet_ntoa(*addr_list[i]) );
    return 0;
  }

  return 1;
}

int ConnectToServer(char* host, char* port, int clientSocket, char* buffer){
  struct sockaddr_in serverAddress;
	socklen_t addressSize;
	int serverSocket;
	// char buffer[4096];

	// Create a socket
	serverSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (serverSocket<0) ErrorResponse(clientSocket, buffer, 500, "<HTML><HEAD><TITLE>500 Internal Server Error</TITLE></HEAD><BODY><H1>500 Internal Server Error</H1>socket() failed on remote server</BODY><HTML>");

  if(port==NULL) port="80";

	// Initializing the parameters
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(atoi(port));
	serverAddress.sin_addr.s_addr = inet_addr(host);
	memset(serverAddress.sin_zero, '\0', sizeof serverAddress.sin_zero);


  // printf("Server: %s %s\n",host,port);
	// printf("Waiting for server!\n\n");

	// Connecting to the server
	addressSize = sizeof serverAddress;
	if(connect(serverSocket, (struct sockaddr *) &serverAddress, addressSize)) ErrorResponse(clientSocket, buffer, 500, "<HTML><HEAD><TITLE>500 Internal Server Error</TITLE></HEAD><BODY><H1>500 Internal Server Error</H1>connect() failed on remote server</BODY><HTML>");

  return serverSocket;
}

int ReadAll(int socket, char* buffer){
  int bytesRead, size=0;
  while(bytesRead = recv(socket, buffer+size, BUFFER_SIZE-size, 0) > 0){
    size = size + strlen(buffer);
    if(strstr(buffer,"\r\n\r\n")!=NULL) break;
  }
  return size;
}

int SendRequest(int socket, char* buffer){
  // printf("\n%d Forward: %s\n",socket,buffer);
  send(socket,buffer,BUFFER_SIZE,0);
  return 0;
}

int SendResponse(int clientSocket, int remoteSocket, char* buffer){
  // bzero(buffer, BUFFER_SIZE);
  int size;
  while((size=recv(remoteSocket,buffer,BUFFER_SIZE,0))>0){ // use SEND & RECV
    // printf("Sent: %d\n",size);
    send(clientSocket,buffer,size,0);
    // bzero(buffer, BUFFER_SIZE);
    // sleep(1);
  }
  return 0;
}

int ProcessGETRequest(struct ParsedRequest* http_request, char* buffer, int clientSocket){ // THis process the GET request only
  char ip[16];
  int size=0, remoteSocket;

  ParsedHeader_set(http_request,"Connection","close");
  ParsedHeader_set(http_request,"Server","Vicky");
  ParsedHeader_remove(http_request,"Host");
  http_request->version="HTTP/1.0";
  // printf("XXX0\n");

  if(hostname_to_ip(http_request->host, ip, clientSocket, buffer)==0){
    // printf("XXX1\n");

    remoteSocket = ConnectToServer(ip, http_request->port, clientSocket, buffer);
    // printf("XXX2\n");

    bzero(buffer, BUFFER_SIZE);
    size = size + snprintf(buffer+size,BUFFER_SIZE-size,"%s %s %s\r\n", http_request->method,http_request->path, http_request->version);
    size = size + snprintf(buffer+size,BUFFER_SIZE-size,"Host: %s\r\n",http_request->host);
    // printf("XXX3\n");

    ParsedRequest_unparse_headers(http_request, buffer+strlen(buffer), BUFFER_SIZE);
    // printf("XXX4\n");

    // printf("XXX:\n%s\nXXXX:\n%s\nXXXXX\n", ip, buffer);

    if(SendRequest(remoteSocket,buffer)<0) ErrorResponse(clientSocket, buffer, 500, "<HTML><HEAD><TITLE>500 Internal Server Error</TITLE></HEAD><BODY><H1>500 Internal Server Error</H1></BODY><HTML>");
    // printf("XXX5\n");
    if(SendResponse(clientSocket, remoteSocket, buffer)<0)  ErrorResponse(clientSocket, buffer, 500, "<HTML><HEAD><TITLE>500 Internal Server Error</TITLE></HEAD><BODY><H1>500 Internal Server Error</H1></BODY><HTML>");
    // printf("XXX6\n");
  }
  else ErrorResponse(clientSocket, buffer, 500, "<HTML><HEAD><TITLE>500 Internal Server Error</TITLE></HEAD><BODY><H1>500 Internal Server Error</H1>hostname_to_ip() failed on remote server</BODY><HTML>");

  close(remoteSocket);

  return 0;
}

int ProcessRequest(struct ParsedRequest* http_request, char* buffer, int clientSocket){
	if (!strcmp(http_request->method,"GET")){
		if(ProcessGETRequest(http_request,buffer,clientSocket)!=0) die("GET Request Error\n");
	}
	else{
		ErrorResponse(clientSocket, buffer, 500, "<HTML><HEAD><TITLE>500 Internal Server Error</TITLE></HEAD><BODY><H1>500 Internal Server Error</H1></BODY><HTML>");
	}

	return 0;
}


void ConnectionHandler(void *clientSocketVoid){
	int bytesRead=0, flag=0, size=0, clientSocket = *(int*)clientSocketVoid;
	char buffer[BUFFER_SIZE];
  bzero(buffer, BUFFER_SIZE);

  // printf("Conn Open %d %d\n", clientSocket, threadCount);

  bytesRead = ReadAll(clientSocket, buffer);
//
  // printf("\n%d REQUEST: %s\n",clientSocket,buffer);
  // fflush(stdout);
  if (bytesRead>0){
  	struct ParsedRequest* http_request = ParsedRequest_create();
  	if (ParsedRequest_parse(http_request, buffer, BUFFER_SIZE)<0){ // Error Response on Malformed Request
  		ErrorResponse(clientSocket, buffer, 500, "<HTML><HEAD><TITLE>400 Malformed Request</TITLE></HEAD><BODY><H1>400 Malformed Request</H1>The http request is malformed.</BODY><HTML>");
  		flag = 1;
  	}
  	else {
  		if (ProcessRequest(http_request, buffer, clientSocket)!=0) flag=1;
  	}
  	ParsedRequest_destroy(http_request);
  }

  close(clientSocket);
  // printf("Conn Closed %d %d\n", clientSocket, threadCount);
  // mysem_wait(&sem);
  threadCount--;
  // mysem_post(&sem);
}

int main(int argc, char *argv[]){
	if (argc!=2) die("Port number?\n");
	gethostname(serverName,sizeof(serverName));
	struct sockaddr_in serverAddress;
	struct sockaddr_storage serverStorage;
	socklen_t addressSize;
	int mainSocket, clientSocket, flag, *newSocket;
	char buffer[BUFFER_SIZE];

	// Create a socket
	mainSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (mainSocket<0) die("socket() failed");

	if (setsockopt(mainSocket, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0) die("setsockopt(SO_REUSEADDR) failed");

	// Initializing the parameters
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(atoi(argv[1]));
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	memset(serverAddress.sin_zero, '\0', sizeof serverAddress.sin_zero);

	// Binding to the socket
	if(bind(mainSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress))) die("bind() failed");

	// Listening for clients with upto 5 connections in queue
  if(listen(mainSocket,SOMAXCONN)<0) die("listen() error");
	// if(listen(mainSocket,20)==0)
	// 	printf("Listening\n");
	// else
	// 	printf("Error\n");

	// Accepting the connection from client
	addressSize = sizeof serverStorage;
  sem.v = 1;
// USE FORTINET BECAUSE IT HANDLES MORE REQUEST AND DO RELATIVE URL WITH HOST IN HEADER
	while(1){
    while(threadCount>MAX_PARALLEL_CONNECTION);
    clientSocket = accept(mainSocket, (struct sockaddr *) &serverStorage, &addressSize);
    // printf("Client Accepted %d\n",clientSocket);
		pthread_t clientThread;
		newSocket = malloc(sizeof(int*));
		*newSocket = clientSocket;
		// New thread for new connection
    // mysem_wait(&sem);
		if (pthread_create(&clientThread, NULL, ConnectionHandler, (void*)newSocket) < 0){
      char buffer[1024];
      ErrorResponse(clientSocket, buffer, 500, "<HTML><HEAD><TITLE>500 Internal Server Error</TITLE></HEAD><BODY><H1>500 Internal Server Error</H1>pthread_create() failed on remote server</BODY><HTML>");
    }

    threadCount++;
    // mysem_post(&sem);
		// printf("Connection established %d\n", clientSocket);
		if (pthread_detach(clientThread)!=0){
      char buffer[1024];
      ErrorResponse(clientSocket, buffer, 500, "<HTML><HEAD><TITLE>500 Internal Server Error</TITLE></HEAD><BODY><H1>500 Internal Server Error</H1>pthread_detach() failed on remote server</BODY><HTML>");
    }
    // printf("HERE %d\n", *newSocket);
	}

	return 0;
}
